IATS Aura Field Formatter
-------------------------
This module allows you to output an donation or ecommerce solution using the
Aura [1] service from IATS-Payments [2].

Rather than reducing a site's security by allowing site builders to paste
JavaScript code into the page to load the donation/store form, this provides a
simple display formatter for Link module [3] fields that will output a URL in
one of the following formats with the required JavaScript tag:

https://www.iatspayments.com/AURA/AURA.aspx?PID=PA7C139169976EF104

https://www.iatspayments.com/AURA/AURA.aspx?TID=PACC3CC012050A138A&PROCESSKEY=PA5EFA2A54EC418E4E156D22C0D0890CCB


Credits / Contact
------------------------------------------------------------------------------
Currently maintained by Damien McKenna [4].

Ongoing development is sponsored by Mediacurrent [5].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  http://drupal.org/project/issues/iats_aura_field


References
------------------------------------------------------------------------------
1: http://www.iatspayments.com/
2: http://home.iatspayments.com/products/aura
3: https://www.drupal.org/project/link
4: https://www.drupal.org/u/damienmckenna
5: http://www.mediacurrent.com/
